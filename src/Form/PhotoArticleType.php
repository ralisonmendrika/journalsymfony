<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 03/05/2019
 * Time: 18:03
 */

namespace App\Form;


use App\Entity\PhotoArticle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PhotoArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('imageFile',VichImageType::class,['download_label'=>'Fichier','label'=>false,'required'=>false])
                ->add('alt',TextType::class,['label'=>'Description Photo','required'=>false]) ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=>PhotoArticle::class
        ]) ;
    }
}