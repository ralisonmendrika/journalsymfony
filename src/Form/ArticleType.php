<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 03/05/2019
 * Time: 17:32
 */

namespace App\Form;


use App\Entity\Article;
use App\Entity\Categorie;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('sousTitre')
            ->add('categorie',EntityType::class,[
                'class'=>Categorie::class,
                'choice_label'=>'nom',
                'expanded'=>false,
                'multiple'=>false
            ])
            ->add('details',CKEditorType::class,[
                'config'=>['imageUploadUrl'=>'']
            ])
            ->add('tagsLetter',TextType::class) ;
        $builder->addEventListener(FormEvents::PRE_SET_DATA,function(FormEvent $formEvent){
            $article = $formEvent->getData() ;
            $form = $formEvent->getForm() ;
            if(!$article || null === $article->getId()) {
                $form->add('photoArticles',CollectionType::class,[
                        'entry_type'=>PhotoArticleType::class,
                        'entry_options'=>[
                            'label'=>'Uploader Photo'
                        ]
                    ]) ;
            }
            $form->add('Valider',SubmitType::class,[
                'attr'=>['class'=>'btn-default btn btn-block']
            ]) ;
        }) ;



    }



}