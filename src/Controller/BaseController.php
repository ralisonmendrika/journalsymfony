<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Service\ArticleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController {

    private $articleService ;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService ;
    }

    public function getNavBar()
    {
        return $this->render('fo/base/navbar.html.twig',["categories"=>$this->getCategories()]);
    }

    public function getFooter(){
        return $this->render('fo/base/footer.html.twig',["categories"=>$this->getCategories()]) ;
    }

    public function getBreakingNews(){
        $news = $this->articleService->getBreakingNews() ;
        return $this->render('fo/base/breakingnews.html.twig',["news"=>$news]) ;
    }

    private function getCategories(){
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll() ;
        return $categories ;
    }


}
