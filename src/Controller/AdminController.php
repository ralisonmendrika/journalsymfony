<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Form\ArticleType;
use App\Form\CategorieType;
use App\Service\ArticleService;
use App\Service\CategArticleService;
use App\Service\PhotoService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{

    public function home(){
        return $this->redirectToRoute('admin_home') ;
    }

    public function homepage(PaginatorInterface $paginatorInterface,Request $request)
    {
        $query = $this->getDoctrine()->getRepository(Article::class)->findQueryArticle() ;
        $articlePagination = $paginatorInterface->paginate(
            $query,
            $request->query->getInt('page',1),
            2
        ) ;
        return $this->render('bo/pages/articles.html.twig', [
            'controller_name' => 'AdminController',
            'articlePagination'=>$articlePagination
        ]);
    }

    public function addArticle(Request $request,PhotoService $photoService){
        $article = new Article() ;
        $form = $this->createForm(ArticleType::class,$article) ;
        $form->handleRequest($request) ;
        if($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $article = $photoService->validateImage($article) ;
            $em = $this->getDoctrine()->getManager() ;
            $em->persist($article) ;
            $photoService->setCouverture($article) ;
            $em->flush();
            $this->addFlash('success','Article Créée avec succèss') ;
            return $this->redirectToRoute('admin_get_article',['slug'=>$article->getSlug()]) ;
        }
        return $this->render('bo/pages/addArticle.html.twig',['articleForm'=>$form->createView()]) ;
    }


    public function getArticle($slug,ArticleService $articleService,Request $request) {
        $article = $articleService->getArticleBySlug($slug) ;
        if(!$article) {
            throw $this->createNotFoundException('Article introuvable') ;
        }
        $form = $this->createForm(ArticleType::class,$article) ;
        $form->handleRequest($request)  ;
        if($form->isSubmitted() && $form->isValid()) {
            $article  = $form->getData() ;
            $em = $this->getDoctrine()->getManager() ;
            $em->flush() ;
            $this->addFlash('success','Article modifiée') ;
            return $this->redirectToRoute('admin_get_article',['slug'=>$article->getSlug()]) ;
        }

        return $this->render('bo/pages/article.html.twig',['articleForm'=>$form->createView(),'article'=>$article]) ;
    }

    public function deleteArticle($slug,ArticleService $articleService) {
        $article =  $articleService->getArticleBySlug($slug) ;
        if(!$article)  {
            throw $this->createNotFoundException('Article introuvable') ;
        }
        $em = $this->getDoctrine()->getManager() ;
        $em->remove($article) ;
        $em->flush() ;
        $this->addFlash('success','Article éffacée') ;
        return $this->redirectToRoute('admin_home') ;
    }

    public function getArticlesCateg($slug,CategArticleService $articleService,PaginatorInterface $paginator,Request $request) {
        $query = $articleService->findQArticleSlugCategorie($slug) ;
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page',1),
            3
        );
        return $this->render('bo/pages/articles.html.twig', [
            'controller_name' => 'AdminController',
            'articlePagination'=>$pagination
        ]);
    }

    public function addCategorie(Request $request) {
        $categorie = new Categorie() ;
        $form = $this->createForm(CategorieType::class,$categorie)  ;
        $form->handleRequest($request) ;
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager() ;
            $categorie = $form->getData()  ;
            $em->persist($categorie);
            $em->flush() ;
            $this->addFlash('success','Catégorie ajouté')  ;
            return $this->redirectToRoute('admin_home') ;
        }
        return $this->render('bo/pages/addCategorie.html.twig',['categorieForm'=>$form->createView()]) ;
    }




}
