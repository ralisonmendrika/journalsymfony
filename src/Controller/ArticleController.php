<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 25/04/2019
 * Time: 08:43
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\Categorie;
use App\Service\ArticleService;
use App\Service\CategArticleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class ArticleController extends AbstractController
{

    private $articleService ;
    private $categArticleService;

    public function __construct(ArticleService $articleService,CategArticleService $categArticleService)
    {
        $this->articleService = $articleService ;
        $this->categArticleService = $categArticleService ;
    }

    public function getNewsCateg($slug,Breadcrumbs $breadcrumbs,Request $request) {
        $title = $slug;
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)
                        ->findOneBy(["slug"=>$title]) ;
        if(!$categorie){
            throw $this->createNotFoundException("Categorie Introuvable") ;
        }
        $breadcrumbs->addRouteItem("Accueil","index") ;
        $breadcrumbs->addItem("Categorie : ".$title) ;
        $news = $this->categArticleService->findArticleSlugCategories($title) ;
        return $this->render("fo/pages/categories.html.twig",["categorie"=>$categorie,"title"=>$categorie->getNom(),"news"=>$news
        ,"keywords"=>"Article,Categorie,".$categorie->getNom(),
        "description"=>$categorie->getDescription()]) ;
    }

    public function homepage(Breadcrumbs $breadcrumbs) {
        $breadcrumbs->addItem("Accueil")  ;
        $results = $this->articleService->getArticleHome() ;
     //   var_dump($results) ;
        return $this->render('fo/pages/homepage.html.twig',["title"=>"Mon journal","contents"=>$results,
            "keywords"=>"Journal,News,ExpressNews",
            "description"=>"Mon journal, Journal en ligne"]) ;
    }

    public function getNews($slug,Breadcrumbs $breadcrumbs) {
        //$request = Request::createFromGlobals() ;
        $title   = $slug  ;
        $article = $this->articleService->getArticleBySlug($title) ;
        if(!$article) {
            throw $this->createNotFoundException('Article Introuvable') ;
        }
        $breadcrumbs->addRouteItem("Accueil","index") ;
        $breadcrumbs->addItem($article->getTitre()) ;
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll() ;
        $tagsNames = $article->getTags() ;
        $toStringTag = "article"  ;
        foreach ($tagsNames as $tag) {
            $toStringTag = $toStringTag.",".$tag->getName();
        }
        return $this->render('fo/pages/article.html.twig',["article"=>$article,
            "title"=>$article->getTitre(),
            "categories"=>$categories,
            "keywords"=>$toStringTag.",".$article->getTitre(),
            "description"=>$article->getSousTitre()])  ;
    }
}