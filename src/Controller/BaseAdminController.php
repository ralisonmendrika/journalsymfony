<?php

namespace App\Controller;

use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BaseAdminController extends AbstractController
{

    public function getNavBar()
    {
       $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll() ;

       return $this->render('bo/base/navbar.html.twig',['categories'=>$categories]) ;
    }
}
