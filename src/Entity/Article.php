<?php

namespace App\Entity;

use Beelab\TagBundle\Entity\AbstractTaggable;
use Beelab\TagBundle\Tag\TaggableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Article extends AbstractTaggable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Length(
     *     max = 50,
     *     maxMessage= "Le titre ne doit pas dépasser de {{ limit }} caractères"
     * )
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     * @Assert\Length(
     *     max = 100,
     *     maxMessage = "Le sous titre ne doit pas dépasser de {{ limit }} caractères"
     * )
     */
    private $sousTitre;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $details;

    /**
     * @Gedmo\Slug(fields={"titre"})
     * @ORM\Column(length=100,unique=true)
     */
    private $slug ;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag",cascade={"persist"})
     */
    protected $tags ;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $updateTag ;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PhotoArticle", mappedBy="Article",cascade={"persist"})
     * @Assert\Valid
     */
    private $photoArticles;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePublication;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $lastUpdateAt ;

    private $tagsLetter ;


    public function __construct()
    {
        parent::__construct();
        $this->photoArticles = new ArrayCollection();
        if($this->photoArticles->isEmpty()) {
            for($i=1;$i<=3;$i++) {
                $this->addPhotoArticle(new PhotoArticle());
            }
        }
        $this->datePublication = new \DateTime() ;
        $this->lastUpdateAt = new \DateTime() ;
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getSousTitre(): ?string
    {
        return $this->sousTitre;
    }

    public function setSousTitre(string $sousTitre): self
    {
        $this->sousTitre = $sousTitre;

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection|PhotoArticle[]
     */
    public function getPhotoArticles(): Collection
    {
        return $this->photoArticles;
    }

    public function addPhotoArticle(PhotoArticle $photoArticle): self
    {
        if (!$this->photoArticles->contains($photoArticle)) {
            $this->photoArticles[] = $photoArticle;
            $photoArticle->setArticle($this);
        }

        return $this;
    }

    public function removePhotoArticle(PhotoArticle $photoArticle): self
    {
        if ($this->photoArticles->contains($photoArticle)) {
            $this->photoArticles->removeElement($photoArticle);
            // set the owning side to null (unless already changed)
            if ($photoArticle->getArticle() === $this) {
                $photoArticle->setArticle(null);
            }
        }

        return $this;
    }

    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->datePublication;
    }

    public function setDatePublication(\DateTimeInterface $datePublication): self
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }





    public function getLastUpdateAt(): ?\DateTimeInterface
    {
        return $this->lastUpdateAt;
    }

    public function setLastUpdateAt(\DateTimeInterface $lastUpdateAt): self
    {
        $this->lastUpdateAt = $lastUpdateAt;

        return $this;
    }

    public function getPhotoCouverture() : ?PhotoArticle {
        $photos = $this->getPhotoArticles() ;
        foreach ($photos as $photo) {
            if($photo->getCouverture()) {
                return $photo ;
            }
        }
        return null ;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt() {
        $this->datePublication = new \DateTime() ;
        $this->lastUpdateAt = new \DateTime() ;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setLastUpdate(){
        $this->lastUpdateAt = new \DateTime() ;
    }

    public function setTagsText(?string $tagsText): void
    {
        $this->updateTag = new \DateTimeImmutable()  ;
        $tagsArray = explode(",",$tagsText) ;
        $tagArrayObject = new ArrayCollection() ;
        $this->tags = new ArrayCollection() ;
        foreach ($tagsArray as $tagA) {
            if(empty($tagA)) {
                $tag = new Tag() ;
                $tag->setName($tag) ;
                $this->tags[] = $tag ;
            }
        }
        parent::setTagsText($tagsText); // TODO: Change the autogenerated stub

    }

    public function getUpdateTag(): ?\DateTimeInterface
    {
        return $this->updateTag;
    }

    public function setUpdateTag(?\DateTimeInterface $updateTag): self
    {
        $this->updateTag = $updateTag;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTagsLetter()
    {
        return $this->getTagsText() ;
    }

    /**
     * @param mixed $tagsLetter
     */
    public function setTagsLetter($tagsLetter): void
    {
        $this->tagsLetter = $tagsLetter;
        $this->setTagsText($tagsLetter) ;
      //  $this->setTags
    }






}
