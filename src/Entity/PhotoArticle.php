<?php

namespace App\Entity;

use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PhotoArticleRepository")
 * @Vich\Uploadable
 */
class PhotoArticle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @Vich\UploadableField(mapping="photo_article", fileNameProperty="url")
     * @Assert\Image(
     *     maxWidth = 500,
     *     maxHeight = 500
     * )
     */
    private $imageFile;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $alt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $couverture ;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Article", inversedBy="photoArticles")
     * @ORM\JoinColumn(nullable=true,onDelete="Cascade")
     */
    private $Article;



    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * PhotoArticle constructor.
     * @param $id
     */
    public function __construct()
    {
        if(!$this->imageFile) {
            $this->imageFile = new File("",false) ;
        }
        $this->couverture = false ;
        $this->setUpdateAt(new \DateTimeImmutable()) ;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url=null): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->Article;
    }

    public function setArticle(?Article $Article): self
    {
        $this->Article = $Article;

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     * @throws \Exception
     */
    public function setImageFile(?File $imageFile=null): void
    {
        $this->imageFile = $imageFile;
        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getCouverture(): ?bool
    {
        return $this->couverture;
    }

    public function setCouverture(bool $couverture): self
    {
        $this->couverture = $couverture;

        return $this;
    }


}
