<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 04/05/2019
 * Time: 06:22
 */

namespace App\Service;


use App\Entity\Article;

class PhotoService
{

    public function validateImage(Article $article) : Article {
        $photos = $article->getPhotoArticles() ;
        foreach ($photos as $photo) {
            if(!$photo->getImageFile()) {
                $article->removePhotoArticle($photo) ;
            }
        }

        return $article ;
    }

    public function setCouverture(Article $article){
        $photos = $article->getPhotoArticles();
        foreach ($photos as $photo) {
            if($photo!=null && $photo->getUrl()!=null) {
                $photo->setCouverture(true) ;
                return ;
            }
        }
    }
}