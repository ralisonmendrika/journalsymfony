<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 26/04/2019
 * Time: 12:48
 */

namespace App\Service;


use App\Entity\Article;
use App\Entity\Categorie;
use Doctrine\ORM\EntityManagerInterface;

class ArticleService
{
    private $em ;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em =$entityManager ;
    }



    public function getArticleHome(){
        $categories = $this->em->getRepository(Categorie::class)->findAll()  ;
        $results    = [] ;
        foreach ($categories as $category) {
            $results[$category->getNom()] = $this->getHomePerCategorie($category)  ;
        }
        return $results ;
    }

    public function getArticleBySlug ($slug) {
        $articleRepo = $this->em->getRepository(Article::class) ;
        $article     = $articleRepo->findOneBy(["slug"=>$slug])  ;
        return $article ;
    }

    public function getBreakingNews() {
        $articleRepo = $this->em->getRepository(Article::class) ;
        $articles    = $articleRepo->findBy([],["datePublication"=>"DESC"],2,0) ;
        return $articles ;
    }

    private function getHomePerCategorie(Categorie $categorie) {
        $articleRepo = $this->em->getRepository(Article::class) ;
        $articles    = $articleRepo->findBy(["categorie"=>$categorie->getId()],["datePublication"=>"DESC"],2,0) ;
        return $articles ;
    }
}