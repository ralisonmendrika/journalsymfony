<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 29/04/2019
 * Time: 14:50
 */

namespace App\Service;


use App\Entity\Article;
use App\Entity\Categorie;
use Doctrine\ORM\EntityManagerInterface;

class CategArticleService
{
    private $em ;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em =$entityManager ;
    }


    public function findArticleSlugCategories ($slugCategorie) {
        $categorie = $this->em->getRepository(Categorie::class)->findOneBy(["slug"=>$slugCategorie]) ;
        if(!$categorie) {
            return null ;
        }
        $articles  = $this->em->getRepository(Article::class)->findByCategorie($categorie->getId()) ;
        return $articles ;
       /* $articles= $this->findArticleSlugCategories($slugCategorie)->getResult() ;
        return $articles ;*/
    }

    public function findQArticleSlugCategorie($slugCategorie) {
        $categorie = $this->em->getRepository(Categorie::class)->findOneBy(["slug"=>$slugCategorie]) ;
        if(!$categorie) {
            return null ;
        }
        $query  = $this->em->getRepository(Article::class)->findQueryCategorie($categorie->getId()) ;
        return $query ;
    }
}