require('../css/bootstrap.css') ;
require('../css/style.css') ;
require('../css/font-awesome.css') ;
require('../css/SidebarNav.min.css') ;
require('../css/custom.css') ;

const $ = require('jquery') ;
global.$ = global.jQuery = $ ;
const classie = require('./classie') ;
const metis   = require('./metisMenu.min') ;

const custom = require('./custom') ;
const navbar = require('./SidebarNav.min') ;
const modern = require('./modernizr.custom') ;

const niceScroll = require('./jquery.nicescroll')  ;
const script = require('./scripts') ;
const boot = require('./bootstrap');



