# Site Journal SYMFONY 4.2 !

## Prérequis 

	 - NodeJS,Yarn pour la concatenation CSS et JS
	 - Composer
	 - Php7

## Installation
	-	Cloner le projet dans gitlab `git clone git@gitlab.com:ralisonmendrika/journalsymfony.git`
	-	Installer les composants nécessaire via composer `composer update`
	-	Mettre à jour votre fichier .env pour configurer votre base 
	-	Creer la base de donnée `php bin/console doctrine:database:create`
	-	Mettre à jour votre base 'php bin/console doctrine:migrations:migrate`
	-	Ajouter les fixture `php bin/console doctrine:fixtures:load`
	-	Démarrer votre serveur de dev `php bin/console server:run`
	-	Acceder votre site à l'adresse(localhost:8000)

